import { Collection } from "mongodb";
import { IResponse, ILayerSchema } from "../@types"; // eslint-disable-line
import createErrorResponse from "../helpers/createErrorResponse";

let gameIsPlaying = false;

export default {
    get gameIsPlaying(): boolean {
        return gameIsPlaying;
    },

    async startGame(db: Collection<ILayerSchema>): Promise<IResponse> {
        gameIsPlaying = true;
        // Delete all the layers:
        const deleteResponse = await db.deleteMany({});

        if (deleteResponse.result.ok !== 1) {
            return createErrorResponse("Unable to clear the game database.");
        }

        return { success: true, data: null, error: null, code: 201 };
    },

    async endGame(db: Collection<ILayerSchema>): Promise<IResponse> {
        gameIsPlaying = false;

        // Delete all the layers:
        const deleteResponse = await db.deleteMany({});

        if (deleteResponse.result.ok !== 1) {
            return createErrorResponse("Unable to clear the game database.");
        }

        return { success: true, data: null, error: null, code: 201 };
    },
};
