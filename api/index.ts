import { CasparCG } from "casparcg-connection";
import { Router } from "express";
import { Collection } from "mongodb";
import { ILayerSchema } from "../@types"; // eslint-disable-line
import { getData, updateData } from "../connection";
import createErrorResponse from "../helpers/createErrorResponse";
import state from "../state";

export default function api(
    connection: CasparCG,
    db: Collection<ILayerSchema>
): Router {
    const router = Router();

    router.get("/getData", async (req, res) => {
        const getResponse = await getData(db);

        res.status(getResponse.code).json(getResponse);
    });

    router.post("/updateData", async (req, res) => {
        try {
            const updateResponse = await updateData(connection, db, req.body);
            res.status(updateResponse.success ? 201 : 500).json(updateResponse);
        } catch (error) {
            res.status(500).json(createErrorResponse(error));
            console.log(error);
        }
    });

    router.get("/isGamePlaying", (req, res) => {
        res.status(200).json({
            success: true,
            data: { isPlaying: state.gameIsPlaying },
        });
    });

    router.post("/startGame", async (req, res) => {
        const startGameResponse = await state.startGame(db);
        res.status(startGameResponse.code).json(startGameResponse);
    });

    router.post("/endGame", async (req, res) => {
        const endGameResponse = await state.endGame(db);
        res.status(endGameResponse.code).json(endGameResponse);
    });

    return router;
}
