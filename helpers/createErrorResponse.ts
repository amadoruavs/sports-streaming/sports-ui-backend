import { IUpdateResponse } from "../@types"; // eslint-disable-line

export default function createErrorResponse(
    error: string,
    code = 500
): IUpdateResponse {
    return {
        success: false,
        data: null,
        error,
        code,
    };
}
