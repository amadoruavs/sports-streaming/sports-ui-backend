import { IRawUpdateParams, TemplateName } from "../@types"; // eslint-disable-line

const TEMPLATE_LAYERS: Record<TemplateName, number> = {
    "credits/index": 15,
    "score-indicator/index": 16,
    "away-touchdown/index": 17,
    "home-touchdown/index": 18,
    "new-lower-third/index": 19,
    "live-symbol/index": 20,
};

export default class UpdateParams implements IRawUpdateParams {
    templateName: TemplateName; // the template name you want to change

    command: "ADD" | "PLAY" | "STOP" | "UPDATE" | "REMOVE"; // action

    data?: Record<string, unknown>; // data payload (optional)

    autoPlay?: boolean; // autoplay (optional, default = false)

    layer: number;

    constructor({ templateName, command, data, autoPlay }: IRawUpdateParams) {
        this.templateName = `${templateName}/index` as TemplateName;
        this.command = command;
        this.data = data;
        this.autoPlay = autoPlay;
        this.layer = TEMPLATE_LAYERS[this.templateName];
    }
}
