import { Command } from "casparcg-connection";
import { IUpdateResponse } from "../@types"; // eslint-disable-line

export const MESSAGES = Object.freeze({
    200: "OK data returned",
    201: "OK data returned",
    202: "OK",

    400: "ERROR invalid command",
    401: "ERROR illegal video channel",
    402: "ERROR parameter missing",

    500: "FAILED internal server error",
    501: "FAILED internal server error",
    502: "FAILED media file unreadable",
    503: "FAILED access error",
});

export default function createUpdateResponse(
    amcp: Command.IAMCPCommand
): IUpdateResponse {
    const { response } = amcp;
    const code = parseInt(
        response.code.toString().substring(0, 3),
        10
    ) as keyof typeof MESSAGES;
    const success = code.toString().startsWith("20");
    const message = MESSAGES[code] || "Invalid error code";

    const data = success ? { message, payload: response } : null;
    const error = success ? null : message;

    return { success, error, data, code };
}
