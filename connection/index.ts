import { CasparCG } from "casparcg-connection";
import { Collection } from "mongodb";
import {
    IUpdateResponse,
    IRawUpdateParams,
    IResponse,
    ILayerSchema,
} from "../@types"; // eslint-disable-line
import addCommand from "../commands/add";
import playCommand from "../commands/play";
import stopCommand from "../commands/stop";
import updateCommand from "../commands/update";
import createErrorResponse from "../helpers/createErrorResponse";
import UpdateParams from "../helpers/UpdateParams";

export async function getData(
    db: Collection<ILayerSchema>
): Promise<IResponse> {
    const layers = await db.find().toArray();

    return {
        success: true,
        data: { layers: layers || null },
        code: 200,
        error: null,
    };
}

export function updateData(
    connection: CasparCG,
    db: Collection<ILayerSchema>,
    params: IRawUpdateParams
): Promise<IUpdateResponse> {
    const updateParams = new UpdateParams(params);

    switch (updateParams.command) {
        case "ADD":
            return addCommand(connection, db, updateParams);

        case "PLAY":
            return playCommand(connection, db, updateParams);

        case "STOP":
            return stopCommand(connection, db, updateParams);

        case "UPDATE":
            return updateCommand(connection, db, updateParams);

        default:
            return Promise.resolve(
                createErrorResponse(
                    "Command not found, must be ADD, PLAY, REMOVE, STOP, or UPDATE."
                )
            );
    }
}
