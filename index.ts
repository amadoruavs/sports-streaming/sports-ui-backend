import dotenv from "dotenv";
import express from "express";
import morgan from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
import { CasparCG } from "casparcg-connection";
import { MongoClient } from "mongodb";

import api from "./api";
import { ILayerSchema } from "./@types"; // eslint-disable-line

(async () => {
    dotenv.config();

    const connection = new CasparCG({
        host: process.env.CASPAR_HOST as string,
        port: Number(process.env.CASPAR_PORT),
    });

    const client = await MongoClient.connect(process.env.DB_URL as string, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    const db = client.db("db").collection<ILayerSchema>("layerz");

    const app = express();

    app.use(cors());
    app.use(bodyParser.json());
    app.use(morgan("dev"));

    app.use("/api", api(connection, db));

    const port = process.env.SERVER_PORT || 8080;

    app.listen(port, () => console.log(`Listening at :${port}`));
})();
