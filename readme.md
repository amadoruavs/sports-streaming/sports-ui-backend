# Sports UI Backend:

Basically this lets the frontend talk to CasparCG using HTTP requests.

## `.env` File:

```bash
SERVER_PORT=port to run the server
CASPAR_HOST=hostname of caspar server
CASPAR_PORT=port that caspar runs at
DB_URL=mongodb connection string
```

## Routes:

-   **`GET`** `/api/getData`:
    -   calling this will return the JSON representation of the current CasparCG state (layers, transition state, data)
-   **`POST`** `/api/updateData`:
    -   calling this with the JSON body of `IRawUpdateParams` will let you modify Caspar stuff.

## `/getData` Response Format:

```ts
export interface IResponse {
    success: boolean; // success lol
    data: Record<string, unknown> | null; // the actual data or null
    error: string | null; // error message or null
    code: number; // status code
}
```

## `/updateData` Request Format:

```ts
export interface IRawUpdateParams {
    layer: number; // the layer number to modify.
    templateName: TemplateName; // the template name you want to change
    command: "ADD" | "PLAY" | "STOP" | "UPDATE" | "REMOVE"; // action
    data?: Data; // data payload (optional)
    autoPlay?: boolean; // autoplay (optional, default = false)
}
```

## `/updateData` Response Format

```ts
export interface IUpdateResponse {
    success: boolean; // success lol
    payload: { message: string; payload: Command.IAMCPResponse } | null; // detailed response or null
    error: string | null; // error message or null
    code: number; // status code
}
```
