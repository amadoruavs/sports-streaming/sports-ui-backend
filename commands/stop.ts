import { CasparCG } from "casparcg-connection";
import { Collection } from "mongodb";
import { ILayerSchema, IUpdateResponse } from "../@types"; // eslint-disable-line
import createErrorResponse from "../helpers/createErrorResponse";
import createUpdateResponse from "../helpers/createUpdateResponse";
import UpdateParams from "../helpers/UpdateParams";

export default async function stop(
    connection: CasparCG,
    db: Collection<ILayerSchema>,
    params: UpdateParams
): Promise<IUpdateResponse> {
    console.log("STOP");

    const getResponse = await db.findOne({ layer: params.layer });

    if (getResponse && getResponse.playing) {
        const updateResponse = await db.replaceOne(
            { layer: params.layer },
            {
                data: { ...(getResponse.data || {}), ...params.data },
                layer: params.layer,
                templateName: params.templateName,
                playing: false,
            }
        );

        if (updateResponse.result.ok !== 1) {
            return createErrorResponse("Unable to update data");
        }

        return createUpdateResponse(
            await connection.cgStop(1, params.layer, 0)
        );
    }

    return {
        success: true,
        data: null,
        error: null,
        code: 201,
    };
}
