import { CasparCG } from "casparcg-connection";
import { Collection } from "mongodb";
import { ILayerSchema, IUpdateResponse } from "../@types"; // eslint-disable-line
import createErrorResponse from "../helpers/createErrorResponse";
import createUpdateResponse from "../helpers/createUpdateResponse";
import UpdateParams from "../helpers/UpdateParams";
import addCommand from "./add";

export default async function update(
    connection: CasparCG,
    db: Collection<ILayerSchema>,
    params: UpdateParams
): Promise<IUpdateResponse> {
    console.log("UPDATE");

    const getResponse = await db.findOne({ layer: params.layer });

    if (getResponse) {
        const updateResponse = await db.replaceOne(
            { layer: params.layer },
            {
                layer: params.layer,
                data: { ...getResponse.data, ...params.data },
                templateName: params.templateName,
                playing: getResponse.playing,
            }
        );

        if (updateResponse.result.ok !== 1) {
            return createErrorResponse("Unable to update data");
        }

        return createUpdateResponse(
            await connection.cgUpdate(1, params.layer, 0, params.data || {})
        );
    }

    // Layer doesn't exist, so create it!
    return addCommand(connection, db, params);
}
