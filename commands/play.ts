import { CasparCG } from "casparcg-connection";
import { Collection } from "mongodb";
import { ILayerSchema, IUpdateResponse } from "../@types"; // eslint-disable-line
import createErrorResponse from "../helpers/createErrorResponse";
import createUpdateResponse from "../helpers/createUpdateResponse";
import UpdateParams from "../helpers/UpdateParams";
import addCommand from "./add";
import updateCommand from "./update";

export default async function play(
    connection: CasparCG,
    db: Collection<ILayerSchema>,
    params: UpdateParams
): Promise<IUpdateResponse> {
    console.log("PLAY");

    const getResponse = await db.findOne({ layer: params.layer });

    if (getResponse) {
        const updateResponse = await db.replaceOne(
            { layer: params.layer },
            {
                data: { ...(getResponse.data || {}), ...params.data },
                layer: params.layer,
                templateName: params.templateName,
                playing: true,
            }
        );

        if (updateResponse.result.ok !== 1) {
            return createErrorResponse("Unable to update data");
        }

        if (params.data) {
            await updateCommand(connection, db, params);
        }

        const playResponse = createUpdateResponse(
            await connection.cgPlay(1, params.layer, 0)
        );

        return playResponse;
    }

    // Layer doesn't exist, so create it!
    return addCommand(connection, db, { ...params, autoPlay: true });
}
