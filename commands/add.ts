import { CasparCG } from "casparcg-connection";
import { Collection } from "mongodb";
import { ILayerSchema, IRawUpdateParams, IUpdateResponse } from "../@types"; // eslint-disable-line
import createUpdateResponse from "../helpers/createUpdateResponse";
import UpdateParams from "../helpers/UpdateParams";

export default async function add(
    connection: CasparCG,
    db: Collection<ILayerSchema>,
    params: UpdateParams
): Promise<IUpdateResponse> {
    console.log("ADD");

    const res = createUpdateResponse(
        await connection.cgAdd(
            1,
            params.layer,
            0,
            params.templateName,
            params.autoPlay || false,
            params.data
        )
    );

    await db.deleteOne({ layer: params.layer });
    await db.insertOne({
        layer: params.layer,
        templateName: params.templateName,
        data: params.data || {},
        playing: params.autoPlay || false,
    });

    return res;
}
