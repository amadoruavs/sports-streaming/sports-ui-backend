import { Command } from "casparcg-connection";

export type TemplateName =
    | "away-touchdown/index"
    | "credits/index"
    | "home-touchdown/index"
    | "score-indicator/index"
    | "new-lower-third/index"
    | "live-symbol/index";

export interface IRawUpdateParams {
    templateName: TemplateName; // the template name you want to change
    command: "ADD" | "PLAY" | "STOP" | "UPDATE" | "REMOVE"; // action
    data?: Record<string, unknown>; // data payload (optional)
    autoPlay?: boolean; // autoplay (optional, default = false)
}

export interface ILayerSchema {
    layer: number;
    templateName: string;
    data: Record<string, unknown>;
    playing: boolean;
}

export interface IResponse {
    success: boolean; // success lol
    data: Record<string, unknown> | null; // detailed response or null
    error: string | null; // error message or null
    code: number; // status code
}

export interface IUpdateResponse extends IResponse {
    data: { message: string; payload: Command.IAMCPResponse } | null; // detailed response or null
}
